CREATE TABLE `tc` (
  `tc_id` int(11) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `description` varchar(4000) DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `user_created` varchar(200) DEFAULT NULL,
  `is_deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `tc_execution` (
  `tc_execution` int(11) NOT NULL,
  `start_date` datetime DEFAULT NULL,
  `end_date` datetime DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `user_started` varchar(200) DEFAULT NULL,
  `host` varchar(1000) DEFAULT NULL,
  `parameters` varchar(4000) DEFAULT NULL,
  PRIMARY KEY (`tc_execution`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `tc_published_data` (
  `tc_id` int(11) NOT NULL,
  `version` int(11) NOT NULL DEFAULT '1',
  `data` blob,
  `date_created` datetime DEFAULT NULL,
  `user_created` varchar(200) DEFAULT NULL,
  `parameters` varchar(4000) DEFAULT NULL,
  PRIMARY KEY (`tc_id`,`version`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `tc_tag` (
  `tc_id` int(11) NOT NULL,
  `tag_name` varchar(50) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `user_created` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`tc_id`,`tag_name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `tc_user_data` (
  `user_tc_id` int(11) NOT NULL,
  `user` varchar(200) NOT NULL DEFAULT 'a',
  `name` varchar(50) DEFAULT NULL,
  `description` varchar(4000) DEFAULT NULL,
  `data` blob,
  `date_created` datetime DEFAULT NULL,
  `parameters` varchar(4000) DEFAULT NULL,
  `published_tc_id` int(11) DEFAULT '-1',
  `published_tc_version` int(11) DEFAULT '-1',
  PRIMARY KEY (`user_tc_id`,`user`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

