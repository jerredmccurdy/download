pentaho.Share = React.createClass
	getInitialState: () ->
		{
			name: @props.project.name
			update: true
		}
	render: () ->
		<div>
			<div>Name:</div>
			<input ref="input" value={@state.name} onChange={@onNameChange}/>
			{ if @props.project.publishedTc >= 0
				<div>
					<div>
						<input type="radio" id="updateTrue" value="true" name="update" checked={@state.update is true} onChange={@onUpdate}/>
						<label htmlFor="updateTrue">Update existing TC</label>
					</div>
				
					<div>
						<input type="radio" id="updateFalse" value="false" name="update" checked={@state.update is false} onChange={@onUpdate}/>
						<label htmlFor="updateFalse">This is a new TC</label>
					</div>
				</div>
			}
			<div>
				<button onClick={@onPublish}>Publish</button>
				<button onClick={@onCancel}>Cancel</button>
			</div>
		</div>
	componentDidMount: () ->
		input = @refs.input
		jer.clock.later 1, () ->
			input.select()
			input.focus()
	onNameChange: (event) ->
		console.log event.target.name
		@setState
			name: event.target.name
	onUpdate: (event) ->
		@setState
			update: event.target.value is "true"
	onPublish: () ->
		self = this
		jer.ajax
			url: "pentaho.json"
			method: "GET"
			binary: false
			params:
				action: "share"
				projectId: @props.project.id
				name: @state.name
				update: @state.update
			success: (result) ->
				self.props.project.publishedTc = result.id
				self.props.project.publishedVersion = result.version
				$("#loading").hide()
				jer.dialog.hideDialog()
	onCancel: () ->
		jer.dialog.hideDialog()