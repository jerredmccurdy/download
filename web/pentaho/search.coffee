( () ->
	Project = React.createClass
		render: () ->
			project = @props.project
			<div>
				<div>{project.name}</div>
				<div><button onClick={@onClick}>Open TC</button></div>
			</div>
		onClick: () ->
			@props.onClick @props.project

	OpenProject = React.createClass
		getInitialState: () ->
			name: @props.project.name
		render: () ->
			# TODO Check that the name is not blank
			# TODO Check that the name does not already exist
			<div>
				<div>
					Create a name for this TC.
				</div>
				<div>
					<input ref="input" autoFocus value={@state.name} onChange={@onChange}/>
				</div>
				<div>
					<button onClick={@onCreate}>Create</button>
					<button onClick={@onCancel}>Cancel</button>
				</div>
			</div>
		componentDidMount: () ->
			self = this
			jer.clock.later 1, () ->
				ReactDOM.findDOMNode(self.refs.input).focus()
		onChange: (event) ->
			@setState
				name: event.target.value
		onCreate: () ->
			self = this
			$("#loading").show()
			jer.ajax
				url: "pentaho.json"
				method: "GET"
				binary: false
				params:
					action: "openPublishedProject"
					name: @state.name
					id: @props.project.id
				success: (result) ->
					jer.dialog.hideDialog()
					console.log self.props
					self.props.onProjectLoaded result.id
		onCancel: () ->
			jer.dialog.hideDialog()

	this.pentaho.Search = React.createClass
		getInitialState: () ->
			{
			}
		render: () ->
			console.log @props.projects
			<div>
				{ <Project project={project} key={project.id} onClick={@onClick}/> for project in @props.projects}
			</div>
		onClick: (project) ->
			jer.dialog.showDialog <OpenProject project={project} onProjectLoaded={@props.onProjectLoaded}/>
		
			
)(this)