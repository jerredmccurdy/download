pentaho.NewProjectOrSearchProject = React.createClass
	render: () ->
		<div>
			<div>
				You do not have any projects in Pentaho.
			</div>
			<div>
				<button onClick={@newProject}>Create new project</button>
			</div>
			<div>
				<button onClick={@searchProjects}>Search for published project</button>
			</div>
		</div>
	newProject: () ->
		jer.dialog.hideDialog()
		@props.newProject()
	searchProjects: () ->
		jer.dialog.hideDialog()
		@props.searchProject()

pentaho.NewFile = React.createClass
	getInitialState: () ->
		name: ""
	render: () ->
		<div>
			<div>
				Name your transform:
			</div>
			<div>
				<input ref="input" autoFocus value={@state.name} onChange={@onChange}/>
			</div>
			<div>
				<button onClick={@onCreate}>Create</button>
				<button onClick={@onCancel}>Cancel</button>
			</div>
		</div>
	componentDidMount: () ->
		self = this
		jer.clock.later 1, () ->
			ReactDOM.findDOMNode(self.refs.input).focus()
	onChange: (event) ->
		@setState
			name: event.target.value
	onCreate: () ->
		self = this
		jer.dialog.hideDialog()
		jer.ajax
			url: "pentaho.json"
			method: "GET"
			binary: false
			params:
				action: "createFile"
				name: @state.name
				id: @props.projectId
				type: @props.type
			success: (result) ->
				$("#loading").hide()
				self.props.onFileCreated()
	onCancel: () ->
		jer.dialog.hideDialog()
		
pentaho.NewProject = React.createClass
	getInitialState: () ->
		name: ""
	render: () ->
		# TODO Check that the name is not blank
		# TODO Check that the name does not already exist
		<div>
			<div>
				Create a name for your project.
			</div>
			<div>
				<input ref="input" autoFocus value={@state.name} onChange={@onChange}/>
			</div>
			<div>
				<button onClick={@onCreate}>Create</button>
				<button onClick={@onCancel}>Cancel</button>
			</div>
		</div>
	componentDidMount: () ->
		self = this
		jer.clock.later 1, () ->
			ReactDOM.findDOMNode(self.refs.input).focus()
	onChange: (event) ->
		@setState
			name: event.target.value
	onCreate: () ->
		self = this
		$("#loading").show()
		jer.ajax
			url: "pentaho.json"
			method: "GET"
			binary: false
			params:
				action: "createProject"
				name: @state.name
			success: (result) ->
				jer.dialog.hideDialog()
				self.props.onProjectCreated result.id
	onCancel: () ->
		jer.dialog.hideDialog()