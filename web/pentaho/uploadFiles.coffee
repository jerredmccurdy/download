pentaho.UploadFiles = React.createClass
	render: () ->
		<div>
			<div>Drop your files below.</div>
			<ReactFileDrop className="fileDrop" frame={document} onDrop={@onFileDrop}/>
			<div>
				<button onClick={@onOkay}>Okay</button>
			</div>
		</div>
	onFileDrop: (files) ->
		self = this
		data = new FormData()
		$.each files, (key, value) ->
			data.append(key, value)
		$.ajax
			url: "http://localhost:8080/" + "pentaho.json?projectId=" + @props.projectId
			type: "POST"
			data: data
			dataType: "json"
			cache: false
			contentType: false
			processData: false
			success: (data) ->
				self.props.onFileUploaded()
	onOkay: () ->
		jer.dialog.hideDialog()
