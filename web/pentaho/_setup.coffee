pentaho = {}
$ () ->

	Textbox = React.createClass
		getInitialState: () ->
			{
				text: @props.text
			}
		render: () ->
			<input ref="input" type="text" className="fileName" tabIndex="0" value={@state.text} onChange={@onChange} onKeyPress={@onKeyPress} onBlur={@onBlur} onClick={@onClick}/>
		componentDidMount: () ->
			@refs.input.select()
			@refs.input.focus()
		onChange: (event) ->
			@setState
				text: event.target.value
		onKeyPress: (event) ->
			if event.key is "Enter"
				@props.onChange @state.text
		onBlur: (event) ->
			if event.target.value isnt @props.text
				@props.onChange @state.text
			else
				@props.onLeave()
		onClick: (event) ->
			event.preventDefault()
			event.stopPropagation()

	File = React.createClass
		getInitialState: () ->
			{
				edit: false
				name: @props.file.name
			}
		render: () ->
			file = @props.file

			icon = "images/"
			if file.type is 1
				icon += "transform.png"
			else if file.type is 2
				icon += "job.png"
			else
				icon += "file.png"

			c = "file"
			if @props.selected is true
				c += " selected"

			<div className={c} ref="div" tabIndex="0" onClick={@onClick} onDoubleClick={@onDoubleClick}  onClick={@onClick} onKeyDown={@onKeyDown}>
				<img src={icon} className="icon"/>
				{ if @props.selected and @state.edit
					<Textbox text={@state.name} onChange={@onChange} onLeave={@onLeave}/>
				else
					<span>{@state.name}</span>
				}
			</div>
		onClick: () ->
			@props.onFileClick @props.file
			@refs.div.focus()
		onDoubleClick: () ->
			@setState
				edit: true
		onKeyDown: (event) ->
			if event.keyCode is 8
				@props.onDelete @props.file
		
		onChange: (text) ->
			self = this
			$("#loading").show()
			jer.ajax
				url: "pentaho.json"
				method: "GET"
				binary: false
				params:
					action: "rename"
					projectId: @props.projectId
					from: self.props.file.name
					to: text
					type: self.props.file.type
				success: (result) ->
					$("#loading").hide()
					if result.status is "success"
						# Rename
						files = result.list
						self.props.file.name = text
						self.setState
							edit: false
							name: text
					else if result.status is "exists"
						jer.notify.error "Name already exists"
					else
						jer.notify.error "Error"
		onLeave: () ->
			@setState
				edit: false


	Action = React.createClass
		getDefaultProps: () ->
			{
				enabled: true
			}
		render: () ->
			imgSrc = "images/#{@props.image}"
			if @props.enabled
				imgSrc += ".png"
				c = "action_enabled"
			else
				imgSrc += "_no.png"
				c = "action_disabled"
			<div className={c} onClick={@onClick}>
				<div><img src={imgSrc} className="icon"/></div>
				<div>{@props.name}</div>
			</div>
		onClick: (event) ->
			if @props.enabled is true
				@props.onClick(event)


	Project = React.createClass
		getInitialState: () ->
			self = this
			@loadProjects()
			{
				selectedProjectId: @props.selectedProjectId
				projects: null
				selectedFile: null
				files: []
				projectName: null
			}
		render: () ->
			# Create blank select if no project selected
			projects = []
			if @state.projects?
				for p in @state.projects
					projects.push p
			hasSelectedProject = false
			for project in projects
				if project.id is @state.selectedProjectId
					hasSelectedProject = true
			if hasSelectedProject
				selectedProjectId = @state.selectedProjectId
			else
				selectedProjectId = -1
				projects.push
					id: -1
					name: ""

			hasUpdate = false
			

			<div id="project">
				<select id="projectSelect" value={selectedProjectId} onChange={@onProjectChanged}>
					{
						<option key={project.id} value={project.id}>{project.name}</option> for project in projects
					}
					<option value="new">*** Create New Project ***</option>
					<option value="search">*** Search for Projects ***</option>
				</select>
				<div className="projectActions">
					<Action name="Update" image="update" onClick={@onUpdate} enabled={hasUpdate}/>
					<Action name="Share" image="share" onClick={@onShare}/>
					<Action name="Details" image="info" onClick={@onDetails}/>
				</div>
				<div>
					{
						<File file={file} key={file.name} projectId={@state.selectedProjectId} onFileClick={@onFileClick} onDelete={@onDelete} selected={file is @state.selectedFile}/> for file in @state.files
					}
				</div>
				<div className="projectActions">
					<Action name="Transform" image="transform" onClick={@onCreateTransform}/>
					<Action name="Job" image="job" onClick={@onCreateJob}/>
					<Action name="Upload" image="upload" onClick={@onUpload}/>
				</div>
			</div>
		componentDidMount: () ->
			if @props.selectedProjectId?
				@loadProject @props.selectedProjectId
		onUpdate: () ->
			jer.ajax
				url: "pentaho.json"
				method: "GET"
				binary: false
				params:
					action: "update"
				success: (result) ->
					$("#loading").hide()
		onShare: () ->
			jer.dialog.showDialog <pentaho.Share project={@getProject()}/>
		onDetails: () ->
			console.log "Details"
		onFileClick: (file) ->
			@setState
				selectedFile: file
		onCreateTransform: () ->
			jer.dialog.showDialog <pentaho.NewFile projectId={@state.selectedProjectId} type={1} onFileCreated={@loadProject}/>
		onCreateJob: () ->
			jer.dialog.showDialog <pentaho.NewFile projectId={@state.selectedProjectId} type={2} onFileCreated={@loadProject}/>
		onUpload: () ->
			jer.dialog.showDialog <pentaho.UploadFiles projectId={@state.selectedProjectId} onFileUploaded={@loadProject}/>
		onDelete: (deleteFile) ->
			self = this
			$("#loading").show()
			jer.ajax
				url: "pentaho.json"
				method: "GET"
				binary: false
				params:
					action: "delete"
					projectId: @state.selectedProjectId
					name: deleteFile.name
					type: deleteFile.type
				success: (result) ->
					$("#loading").hide()
					if result.status is "success"
						# Delete
						newFiles = self.state.files.filter (f) ->
							f isnt deleteFile
						self.setState
							files: newFiles
					else
						jer.notify.error "Error"
					
				event.preventDefault()
		loadProjects: () ->
			self = this
			$("#loading").show()
			jer.ajax
				url: "pentaho.json"
				method: "GET"
				binary: false
				params:
					action: "getProjects"
				success: (result) ->
					$("#loading").hide()
					self.setState
						projects: result.projects
					if result.projects.length is 0
						jer.dialog.showDialog <pentaho.NewProjectOrSearchProject newProject={self.newProject} searchProject={self.searchProject}/>
		loadProject: (id) ->
			self = this
			if not id?
				id = @state.selectedProjectId
			localStorage.setItem "selectedProjectId", id
			@setState
				selectedProjectId: id
				files: []
				selectedFile: null
			$("#loading").show()
			jer.ajax
				url: "pentaho.json"
				method: "GET"
				binary: false
				params:
					action: "listFiles"
					projectId: id
				success: (result) ->
					$("#loading").hide()
					self.setState
						files: result.files
						projectName: result.name
		newProject: () ->
			jer.dialog.showDialog <pentaho.NewProject onProjectCreated={@onProjectCreated}/>
		onProjectCreated: (id) ->
			@loadProjects()
			@loadProject id
		searchProject: () ->
			self = this
			$("#loading").show()
			onProjectLoaded = (id) ->
				self.loadProjects()
				self.loadProject id
			jer.ajax
				url: "pentaho.json"
				method: "GET"
				binary: false
				params:
					action: "getPublishedTcs"
				success: (result) ->
					$("#loading").hide()
					projects = result.projects
					jer.dialog.showDialog <pentaho.Search onProjectLoaded={onProjectLoaded} projects={projects}/>
		onProjectChanged: (event) ->
			id = event.target.value
			if id is "new"
				@newProject()
			else if id is "search"
				@searchProject()
			else
				@loadProject parseInt(id)
		getProject: () ->
			for project in @state.projects
				if project.id is parseInt(@state.selectedProjectId)
					return project
			return null

	refresh = () ->
		id = localStorage.getItem "selectedProjectId"
		if id?
			id = parseInt id
		ReactDOM.render(<Project selectedProjectId={id}/>, document.getElementById('pentaho'))
	refresh()
	

