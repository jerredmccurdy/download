package pentaho;

import java.util.Date;

public class UserProject {
	private int id;
	private String name, description;
	private int publishedTc;
	private int publishedVersion;
	private Date dateCreated;
	private Parameters parameters;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public Parameters getParameters() {
		return parameters;
	}

	public void setParameters(Parameters parameters) {
		this.parameters = parameters;
	}

	public int getPublishedTc() {
		return publishedTc;
	}

	public void setPublishedTc(int publishedTc) {
		this.publishedTc = publishedTc;
	}

	public int getPublishedVersion() {
		return publishedVersion;
	}

	public void setPublishedVersion(int publishedVersion) {
		this.publishedVersion = publishedVersion;
	}

	public boolean hasPublished() {
		return publishedTc >= 0;
	}

}
