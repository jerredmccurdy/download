package pentaho;

import java.beans.PropertyVetoException;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import com.mchange.v2.c3p0.ComboPooledDataSource;

public class Dao {
	private static ComboPooledDataSource databasePool;
	Connection conn = null;

	public Dao() {
		conn = getConnection();
	}

	public int createTc(String name, String description, String user) {
		try {
			int tcId = getNextSequence("tc", "tc_id");
			PreparedStatement stmt = conn.prepareStatement("insert into tc (tc_id, name, description, date_created, user_created) values (?,?,?,?,?)");
			stmt.setInt(1, tcId);
			stmt.setString(2, name);
			stmt.setString(3, description);
			setTime(stmt, 4, new Date());
			stmt.setString(5, user);
			stmt.execute();
			stmt.close();
			return tcId;
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	public int createUserTc(String user, String name) {
		try {
			int id = getNextSequence("tc_user_data", "user_tc_id");
			PreparedStatement stmt = conn.prepareStatement("insert into tc_user_data (user_tc_id, user, name, date_created) values (?,?,?,?)");
			stmt.setInt(1, id);
			stmt.setString(2, user);
			stmt.setString(3, name);
			setTime(stmt, 4, new Date());
			stmt.execute();
			stmt.close();
			return id;

		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	public void setUserPublished(int userId, int publishedId, int version) {
		try {
			PreparedStatement stmt = conn.prepareStatement("UPDATE tc_user_data SET published_tc_id=?, published_tc_version=? WHERE user_tc_id=?");
			stmt.setInt(1, publishedId);
			stmt.setInt(2, version);
			stmt.setInt(3, userId);
			stmt.execute();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	public List<UserProject> getUserTcs(String user) {
		ArrayList<UserProject> tcs = new ArrayList<>();
		try {
			PreparedStatement stmt = conn.prepareStatement("SELECT user_tc_id, name, description, date_created, parameters, published_tc_id, published_tc_version FROM pentaho.tc_user_data WHERE user=?");
			stmt.setString(1, user);
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				UserProject projectData = loadUserProject(rs);
				tcs.add(projectData);
			}
			rs.close();
			stmt.close();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		return tcs;
	}

	public Project getPublishedTc(int projectId) {
		Project projectData = null;
		try {
			PreparedStatement stmt = conn
					.prepareStatement("SELECT t.tc_id, name, description, t.date_created, t.user_created, max(d.version) latest_version FROM pentaho.tc t inner join pentaho.tc_published_data d on t.tc_id = d.tc_id WHERE t.tc_id=? GROUP BY t.tc_id, name, description, t.date_created, t.user_created");
			stmt.setInt(1, projectId);
			ResultSet rs = stmt.executeQuery();
			if (rs.next()) {
				projectData = loadProject(rs);
			}
			rs.close();
			stmt.close();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		return projectData;
	}

	public Project getPublishedTc(String name) {
		Project projectData = null;
		try {
			PreparedStatement stmt = conn.prepareStatement(
					"SELECT t.tc_id, name, description, t.date_created, t.user_created, max(d.version) latest_version FROM pentaho.tc t inner join pentaho.tc_published_data d on t.tc_id = d.tc_id WHERE lower(t.name)=? GROUP BY t.tc_id, name, description, t.date_created, t.user_created");
			stmt.setString(1, name.toLowerCase());
			ResultSet rs = stmt.executeQuery();
			if (rs.next()) {
				projectData = loadProject(rs);
			}
			rs.close();
			stmt.close();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		return projectData;
	}

	public List<Project> getPublishedTcs() {
		ArrayList<Project> projects = new ArrayList<>();
		try {
			PreparedStatement stmt = conn
					.prepareStatement("SELECT t.tc_id, name, description, t.date_created, t.user_created, max(d.version) latest_version FROM pentaho.tc t inner join pentaho.tc_published_data d on t.tc_id = d.tc_id GROUP BY t.tc_id, name, description, t.date_created, t.user_created");
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				Project project = loadProject(rs);
				projects.add(project);
			}
			rs.close();
			stmt.close();
		} catch (SQLException ex) {
			throw new RuntimeException(ex);
		}
		return projects;
	}

	private Project loadProject(ResultSet rs) throws SQLException {
		Project projectData;
		projectData = new Project();
		projectData.setId(rs.getInt(1));
		projectData.setName(rs.getString(2));
		projectData.setDescription(rs.getString(3));
		projectData.setDateCreated(getTime(rs, 4));
		projectData.setUserCreated(rs.getString(5));
		projectData.setLatestVersion(rs.getInt(6));
		return projectData;
	}

	public UserProject getUserTc(int projectId) {
		UserProject projectData = null;
		try {
			PreparedStatement stmt = conn.prepareStatement("SELECT user_tc_id, name, description, date_created, parameters, published_tc_id, published_tc_version FROM pentaho.tc_user_data WHERE user_tc_id=?");
			stmt.setInt(1, projectId);
			ResultSet rs = stmt.executeQuery();
			if (rs.next()) {
				projectData = loadUserProject(rs);
			}
			rs.close();
			stmt.close();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		return projectData;
	}

	private UserProject loadUserProject(ResultSet rs) throws SQLException {
		UserProject projectData = new UserProject();
		projectData.setId(rs.getInt(1));
		projectData.setName(rs.getString(2));
		projectData.setDescription(rs.getString(3));
		projectData.setDateCreated(rs.getDate(4));
		// TODO get parameters rs.getString(5)
		projectData.setPublishedTc(rs.getInt(6));
		projectData.setPublishedVersion(rs.getInt(7));
		return projectData;
	}

	public void downloadUserTc(int id, File dir) {
		downloadTc(id, dir, "tc_user_data", "user_tc_id");
	}

	public void downloadPublishedTc(int id, File dir) {
		downloadTc(id, dir, "tc_published_data", "tc_id");
	}

	public void downloadTc(int id, File dir, String table, String column) {
		try {
			PreparedStatement stmt = conn.prepareStatement("SELECT data FROM pentaho." + table + " WHERE " + column + "=?");
			stmt.setInt(1, id);
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				Blob blob = rs.getBlob(1);
				if (blob != null) {
					InputStream in = blob.getBinaryStream();
					ZipInputStream zin = new ZipInputStream(in);
					unzip(zin, dir);
					in.close();
				}
			}
			rs.close();
			stmt.close();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	private static void unzip(ZipInputStream zis, File dir) throws IOException {
		final int BUFFER = 1024;
		ZipEntry entry;

		while ((entry = zis.getNextEntry()) != null) {
			if (!entry.isDirectory()) {
				int count;
				byte data[] = new byte[BUFFER];
				// Write the files to the disk
				File file = new File(dir.getAbsolutePath() + "/" + entry.getName());
				File parent = file.getParentFile();
				if (!parent.exists()) {
					parent.mkdirs();
				}
				FileOutputStream fos = new FileOutputStream(file);
				BufferedOutputStream dest = new BufferedOutputStream(fos, BUFFER);
				while ((count = zis.read(data, 0, BUFFER)) != -1) {
					dest.write(data, 0, count);
				}
				dest.flush();
				dest.close();
				zis.closeEntry();
			}
		}
	}

	public void updateTc(int tcId, String name, String description) {
		try {
			PreparedStatement stmt = conn.prepareStatement("update tc set name=?, description=? where tc_id=?");
			stmt.setString(1, name);
			stmt.setString(2, description);
			stmt.setInt(3, tcId);
			stmt.execute();
			stmt.close();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	public void makePublic(int tcId) {
		try {
			PreparedStatement stmt = conn.prepareStatement("update tc set is_public=1 where tc_id=?");
			stmt.setInt(1, tcId);
			stmt.execute();
			stmt.close();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	public boolean doesPublicTcExist(String name) {
		boolean exists = false;
		try {
			PreparedStatement stmt = conn.prepareStatement("select 1 from tc where name=? and is_public=1");
			ResultSet rs = stmt.executeQuery();
			exists = rs.next();
			rs.close();
			stmt.close();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		return exists;
	}

	public void saveUserTc(int id, File dir) {
		InputStream in;
		try {
			in = zip(dir);
			saveUserTc(id, in);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}

	}

	public void savePublishedTc(int id, int version, String user, String parameters, File dir) {
		InputStream input;
		try {
			input = zip(dir);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}

		try {
			PreparedStatement stmt = conn.prepareStatement("insert into tc_published_data (tc_id, version, date_created, user_created, parameters, data) values (?,?,?,?,?,?)");
			stmt.setInt(1, id);
			stmt.setInt(2, version);
			setTime(stmt, 3, new Date());
			stmt.setString(4, user);
			stmt.setString(5, parameters);
			stmt.setBlob(6, input);
			stmt.execute();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	private static InputStream zip(final File dir) throws IOException {
		Path folder = dir.toPath();
		ByteArrayOutputStream buffer = new ByteArrayOutputStream();

		ZipOutputStream zos = new ZipOutputStream(buffer);
		Files.walkFileTree(folder, new SimpleFileVisitor<Path>() {
			public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
				zos.putNextEntry(new ZipEntry(folder.relativize(file).toString()));
				Files.copy(file, zos);
				zos.closeEntry();
				return FileVisitResult.CONTINUE;
			}

			public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
				zos.putNextEntry(new ZipEntry(folder.relativize(dir).toString() + "/"));
				zos.closeEntry();
				return FileVisitResult.CONTINUE;
			}
		});

		byte[] bytes = buffer.toByteArray();
		InputStream inputStream = new ByteArrayInputStream(bytes);
		return inputStream;
	}

	private void saveUserTc(int id, InputStream input) {
		try {
			PreparedStatement stmt = conn.prepareStatement("update tc_user_data set data=? where user_tc_id=?");
			stmt.setBlob(1, input);
			stmt.setInt(2, id);
			stmt.execute();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	protected int getTcVersion(int tcId) {
		final String sql = "select COALESCE(max(version), 0) from tc_version where tc_id=?";
		try {
			int version = 0;
			PreparedStatement stmt = conn.prepareStatement(sql);
			ResultSet rs = stmt.executeQuery();
			if (rs.next()) {
				version = rs.getInt(1);
			}
			rs.close();
			stmt.close();
			return version;
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	public Date getTime(ResultSet rs, int n) throws SQLException {
		Timestamp t = rs.getTimestamp(n);
		Date date = new Date(t.getTime());
		return date;
	}

	public void setTime(PreparedStatement stmt, int n, Date date) throws SQLException {
		setTime(stmt, n, date.getTime());
	}

	public void setTime(PreparedStatement stmt, int n, long time) throws SQLException {
		stmt.setTimestamp(n, new Timestamp(time));
	}

	public int getNextSequence(String tableName, String columnName) throws SQLException {
		final String sql = "select COALESCE(max(" + columnName + "),0) from " + tableName;
		int id = 0;
		PreparedStatement stmt = conn.prepareStatement(sql);
		ResultSet rs = stmt.executeQuery();
		if (rs.next()) {
			id = rs.getInt(1);
		}
		rs.close();
		stmt.close();
		return id + 1;
	}

	private Connection getConnection() {
		if (databasePool == null) {
			databasePool = new ComboPooledDataSource();
			try {
				databasePool.setDriverClass("com.mysql.jdbc.Driver");
			} catch (PropertyVetoException e) {
				throw new RuntimeException(e);
			} // loads the jdbc driver
			databasePool.setJdbcUrl("jdbc:mysql://localhost/pentaho?user=root&password=jer&useUnicode=true&characterEncoding=utf-8");
			// cpds.setUser("dbuser");
			// cpds.setPassword("dbpassword");
		}
		try {
			return databasePool.getConnection();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	public void close() {
		try {
			conn.close();
		} catch (SQLException e) {
		}
	}

	public static void main(String[] args) {
		File dir = new File("/Users/jerredmccurdy/Documents/Pentaho/TEST");
		Dao dao = null;
		try {
			dao = new Dao();
			// dao.saveUserTc(12, dir);
			// dao.getUserTc(12, dir);
		} finally {
			dao.close();
		}
	}
}
