package pentaho;

public class Parameter {
	private String name;
	private int type;
	private int uiElement;
	private String defaultValue;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public int getUiElement() {
		return uiElement;
	}

	public void setUiElement(int uiElement) {
		this.uiElement = uiElement;
	}

	public String getDefaultValue() {
		return defaultValue;
	}

	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}

}
