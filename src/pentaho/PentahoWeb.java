package pentaho;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.lang.reflect.Method;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.tomcat.util.http.fileupload.FileItem;
import org.apache.tomcat.util.http.fileupload.disk.DiskFileItemFactory;
import org.apache.tomcat.util.http.fileupload.servlet.ServletFileUpload;
import org.apache.tomcat.util.http.fileupload.servlet.ServletRequestContext;

import game.WebData;

@WebServlet("/pentaho.json")
public final class PentahoWeb extends HttpServlet {
	private static final String PARENT = "/Users/jerredmccurdy/Documents/Pentaho";

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		WebData web = new WebData();
		web.request = request;
		web.response = response;
		String user = "Jerred";
		int projectId = web.getParameterAsInt("projectId");
		try {
			ServletFileUpload upload = new ServletFileUpload(new DiskFileItemFactory());
			List<FileItem> items = (List<FileItem>) upload.parseRequest(new ServletRequestContext(request));
			for (FileItem item : items) {
				File file = getFile(user, projectId, item.getName(), ProjectFile.OTHER);
				InputStream in = item.getInputStream();
				Files.copy(in, file.toPath());
			}
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
		saveProject(user, projectId);

		HashMap<Object, Object> data = new HashMap<>();
		data.put("status", "success");
		web.writeJson(data);
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String action = request.getParameter("action");
		WebData web = new WebData();
		web.request = request;
		web.response = response;
		try {
			if (action != null) {
				Class[] params = new Class[1];
				params[0] = WebData.class;
				Method method = this.getClass().getMethod(action, params);
				method.invoke(this, web);
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}

	public void getProjects(WebData web) {
		String user = "Jerred";
		List<UserProject> projects = null;
		Dao dao = null;
		try {
			dao = new Dao();
			projects = dao.getUserTcs(user);
		} finally {
			dao.close();
		}
		HashMap<String, Object> data = new HashMap<>();
		data.put("projects", projects);
		web.writeJson(data);
	}

	public void createProject(WebData web) {
		String user = "Jerred";
		String name = web.getParameter("name");
		int id = -1;
		Dao dao = null;
		try {
			dao = new Dao();
			id = dao.createUserTc(user, name);
		} finally {
			dao.close();
		}
		HashMap<String, Object> data = new HashMap<>();
		data.put("id", id);
		web.writeJson(data);
	}

	private static File getProjectFolder(String user, int projectId) {
		return new File(PARENT + "/" + user + "/" + projectId);
	}

	public static boolean deleteDir(File dir) {
		if (dir.isDirectory()) {
			String[] children = dir.list();
			for (int i = 0; i < children.length; i++) {
				boolean success = deleteDir(new File(dir, children[i]));
				if (!success) {
					return false;
				}
			}
		}

		return dir.delete(); // The directory is empty now and can be deleted.
	}

	public void listFiles(WebData web) {
		// Get project
		int projectId = web.getParameterAsInt("projectId");
		String user = "Jerred";
		File dir = getProjectFolder(user, projectId);

		if (dir.exists()) {
			boolean ableToDelete = deleteDir(dir);
			if (!ableToDelete) {
				throw new RuntimeException("Unable to delete project.");
			}
		}
		dir.mkdirs();
		Dao dao = null;
		UserProject userProject = null;
		try {
			dao = new Dao();
			dao.downloadUserTc(projectId, dir);
			userProject = dao.getUserTc(projectId);
		} finally {
			dao.close();
		}

		// List files
		List<ProjectFile> files = new ArrayList<>();
		for (File file : dir.listFiles()) {
			ProjectFile pf = new ProjectFile();
			String name = file.getName();
			if (name.endsWith(".ktr")) {
				pf.setType(ProjectFile.TRANSFORM);
				name = name.substring(0, name.length() - 4);
			} else if (name.endsWith(".kjb")) {
				pf.setType(ProjectFile.JOB);
				name = name.substring(0, name.length() - 4);
			} else {
				pf.setType(ProjectFile.OTHER);
			}
			pf.setName(name);
			files.add(pf);
		}
		HashMap<String, Object> data = new HashMap<>();
		data.put("files", files);
		data.put("name", userProject.getName());
		web.writeJson(data);
	}

	private static File getFile(String user, int projectId, String name, int type) {
		if (type == ProjectFile.TRANSFORM) {
			name += ".ktr";
		} else if (type == ProjectFile.JOB) {
			name += ".kjb";
		}
		System.out.println("GET FILE " + projectId);
		File file = new File(getProjectFolder(user, projectId).getAbsolutePath() + "/" + name);
		return file;
	}

	public void rename(WebData web) {
		String user = "Jerred";
		int projectId = web.getParameterAsInt("projectId");
		String from = web.getParameter("from");
		String to = web.getParameter("to");
		int type = web.getParameterAsInt("type");
		File fromFile = getFile(user, projectId, from, type);
		File toFile = getFile(user, projectId, to, type);

		HashMap<Object, Object> data = new HashMap<>();
		String status = null;
		if (toFile.exists()) {
			status = "exists";
		} else {
			System.out.println("RENAME " + fromFile.getAbsolutePath() + " > " + toFile.getAbsolutePath());
			fromFile.renameTo(toFile);
			status = "success";
			saveProject(user, projectId);
		}
		data.put("status", status);
		web.writeJson(data);
	}

	public void delete(WebData web) {
		String user = "Jerred";
		int projectId = web.getParameterAsInt("projectId");
		String name = web.getParameter("name");
		int fileType = web.getParameterAsInt("type");
		System.out.println("DEL ProjectId = " + projectId);
		File file = getFile(user, projectId, name, fileType);
		file.delete();
		saveProject(user, projectId);
		HashMap<Object, Object> data = new HashMap<>();
		data.put("status", "success");
		web.writeJson(data);
	}

	public void update(WebData web) {
		System.out.println("Update");
		HashMap<Object, Object> data = new HashMap<>();
		data.put("status", "success");
		web.writeJson(data);
	}

	public void share(WebData web) {
		System.out.println("Share");
		String user = "Jerred";
		int projectId = web.getParameterAsInt("projectId");
		String name = web.getParameter("name");
		boolean updateExisting = web.getParameterAsBoolean("update");

		// Get projects from database
		int tcId = 0;
		int version = 1;
		Dao dao = null;
		UserProject userProject = null;
		Project currentPublishedProject = null;
		Project nameConflict = null;
		String status = null;
		try {
			dao = new Dao();
			userProject = dao.getUserTc(projectId);
			if (updateExisting && userProject.getPublishedTc() >= 0) {
				// Get the existing published TC
				currentPublishedProject = dao.getPublishedTc(userProject.getPublishedTc());
			}
			if (currentPublishedProject == null || !currentPublishedProject.getName().equals(userProject.getName())) {
				// Check for a name conflict
				nameConflict = dao.getPublishedTc(userProject.getName());
			}

			if (currentPublishedProject != null && currentPublishedProject.getLatestVersion() != userProject.getPublishedVersion()) {
				// About to overwrite a published TC without the latest version
				status = "notLatestVersion";
			} else if (nameConflict != null) {
				// Name conflict with existing published TC
				status = "nameConflict";
			} else {
				status = "success";
				if (currentPublishedProject != null) {
					version = currentPublishedProject.getLatestVersion() + 1;
				}
				if (version == 1) {
					// New
					tcId = dao.createTc(name, userProject.getDescription(), user);
				} else {
					// Update
					tcId = currentPublishedProject.getId();
					dao.updateTc(currentPublishedProject.getId(), name, userProject.getDescription());
				}
				String parameters = null;
				File dir = getProjectFolder(user, projectId);
				dao.savePublishedTc(tcId, version, user, parameters, dir);

				// Update user TC
				dao.setUserPublished(userProject.getId(), tcId, version);
			}

		} finally {
			dao.close();
		}

		// Return status
		HashMap<Object, Object> data = new HashMap<>();
		data.put("status", status);
		data.put("id", tcId);
		data.put("version", version);
		web.writeJson(data);
	}

	public void createFile(WebData web) {
		String user = "Jerred";
		int projectId = web.getParameterAsInt("id");
		int fileType = web.getParameterAsInt("type");
		String name = web.getParameter("name");

		if (fileType == ProjectFile.TRANSFORM) {
			name += ".ktr";
		} else if (fileType == ProjectFile.JOB) {
			name += ".kjb";
		}
		File file = new File(getProjectFolder(user, projectId).getAbsolutePath() + "/" + name);
		File parent = file.getParentFile();
		if (!parent.exists()) {
			parent.mkdirs();
		}
		try {
			PrintWriter out = new PrintWriter(new FileWriter(file));
			out.print("ABC");
			out.close();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		saveProject(user, projectId);
		HashMap<Object, Object> data = new HashMap<>();
		data.put("status", "success");
		web.writeJson(data);
	}

	public void saveProject(String user, int projectId) {
		File dir = getProjectFolder(user, projectId);
		Dao dao = null;
		try {
			dao = new Dao();
			dao.saveUserTc(projectId, dir);
		} finally {
			dao.close();
		}

	}

	public void getPublishedTcs(WebData web) {
		List<Project> projects = new ArrayList<>();
		Dao dao = null;
		try {
			dao = new Dao();
			projects = dao.getPublishedTcs();
		} finally {
			dao.close();
		}
		HashMap<String, Object> data = new HashMap<>();
		data.put("projects", projects);
		web.writeJson(data);
	}

	public void openPublishedProject(WebData web) {
		String user = "Jerred";
		String name = web.getParameter("name");
		int publishedId = web.getParameterAsInt("id");
		int userId = 0;
		Dao dao = null;
		try {
			dao = new Dao();
			userId = dao.createUserTc(user, name);
			File dir = getProjectFolder(user, userId);
			dir.mkdirs();
			dao.downloadPublishedTc(userId, dir);
			saveProject(user, userId);
		} finally {
			dao.close();
		}

		// Write Data
		HashMap<String, Object> data = new HashMap<>();
		data.put("id", userId);
		web.writeJson(data);
	}

}
