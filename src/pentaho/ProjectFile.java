package pentaho;

public class ProjectFile {
	public static final int TRANSFORM = 1;
	public static final int JOB = 2;
	public static final int OTHER = 3;

	private String name;
	private int type;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

}
