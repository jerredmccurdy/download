package pentaho;

import java.util.List;

public class Parameters {
	private List<Parameter> parameters;
	private String mainTc;

	public List<Parameter> getParameters() {
		return parameters;
	}

	public void setParameters(List<Parameter> parameters) {
		this.parameters = parameters;
	}

	public String getMainTc() {
		return mainTc;
	}

	public void setMainTc(String mainTc) {
		this.mainTc = mainTc;
	}

}
